module.exports = {
   //配置输出路径
   publicPath: './', 
       //引导webpack去找我的入口文件，即不同的main.js
   chainWebpack: config => {
        config.when(process.env.NODE_ENV === 'production', config => {
            config.entry('app').clear().add('./src/main.js')
            config.set('externals', {
                'Vue': 'Vue',
                'ELEMENT': 'ELEMENT',
                'echarts': 'echarts',
            })
        config.plugin('html').tap(args => {
            return args
        })
    })
    config.when(process.env.NODE_ENV === 'development', config => {
        config.entry('app').clear().add('./src/main.js')
        config.plugin('html').tap(args => {
            return args
        })
    })
},
   //去除生产环境的代码镜像-productionSourceMap
   productionSourceMap: false,
}