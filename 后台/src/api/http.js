import axios from 'axios'
import { MessageBox,Message } from 'element-ui'    //引入element的弹框在js文件中不能直接使用需要引入，把this改成组件名MessageBox.不需要使用$符号
//创建一个axios实例
const server = axios.create({
    baseURL: process.env.VUE_APP_API,
    timeout: 5000
})

//请求拦截，添加请求头
server.interceptors.request.use(config => {
    if(localStorage.getItem('token')){
        config.headers.Authorization=localStorage.getItem('token')
    }
    return config

}, error => {
    Promise.reject(error)
})
server.interceptors.response.use(res => {
    // console.log('响应拦截数据',res);  
    const code = res.data.meta.status || 200;
    //获取错误信息
    const msg = res.data.meta.msg 
    if (msg === '无效token') {
      if(flag){
            flag=false;
            MessageBox.confirm('登录状态已过期，您可以继续留在该页面，或者重新登录', '系统提示', {
              confirmButtonText: '重新登录',
              cancelButtonText: '取消',
              type: 'warning'
            }
          ).then(() => { //点击确定执行
            location.href = '/';  //跳转页面
          }).catch(() => {  //点击取消执行,!!!这里要带着catch，捕捉未知错误信息
          
          });
      } 
    }
    else if (code !== 200) {
            Message.error({
              duration:1000,
              message: msg
          })
     }
    return res.data

}, error => {
    console.log('50,error' , error)
    let { message } = error;
    if (message == "Network Error") {
      message = "后端接口连接异常";
    }
    else if (message.includes("timeout")) {
      message = "系统接口请求超时";
    }
    
    Message({
      message: message,
      type: 'error',
      duration: 1000
    })
    return Promise.reject(error)
})
export default server