import http from './http'

// 登录
const getlogin = async (val) => {
    let data = await http({
        method: 'post',
        url: 'login',
        data: val
    })
    return data
}
// 获取左侧列表
const getleft = async () => {
    let data = await http({
        method: 'get',
        url: 'menus',
    })
    return data
}
// 获取用户
const getuser = async (val) => {
    let data = await http({
        method: 'get',
        url: 'users?' + val,
    })
    return data
}
//添加
const postusers = async (val) => {
    let data = await http({
        method: 'post',
        url: 'users',
        data: val
    })
    return data
}
//删除的提交
const deluser = async (val) => {
    console.log(val);
    let data = await http({
        method: 'delete',
        url: `users/${val}`,
    })
    return data
}
//修改状态的提交
const setstatus = async (val) => {
    let data = await http({
        method: 'put',
        url: `users/${val.uid}/state/${val.type}`,
        data: val
    })
    return data
}
//修改的提交
const alteruser = async (val) => {
    let data = await http({
        method: 'put',
        url: `users/${val.id}`,
        data: val.con
    })
    return data
}
//获取角色类别
const getroles = async () => {
    let data = await http({
        method: 'get',
        url: `roles`,
    })
    return data
}
const setroles = async (val) => {
    let data = await http({
        method: 'put',
        url: `users/${val.id}/role`,
        data: val
    })
    return data
}
const getmenus = async () => {
    let data = await http({
        method: 'get',
        url: `menus`,
    })
    return data
}

const addroles = async (val) => {
    let data = await http({
        method: 'post',
        url: `roles`,
        data: val
    })
    return data
}
const upuserRoles = async (val) => {
    let data = await http({
        method: 'put',
        url: `roles/${val.id}`,
        data: val
    })
    return data
}
const delrole = async (val) => {
    let data = await http({
        method: 'delete',
        url: `roles/${val}`,
    })
    return data
}
const delroledes = async (val) => {
    let data = await http({
        method: 'delete',
        url: `roles/${val.id}/rights/${val.rid}`,
    })
    return data
}
const getdesAll = async () => {
    let data = await http({
        method: 'get',
        url: `rights/tree`,
    })
    return data
}
const setdesall = async (val) => {
    console.log(val);
    let data = await http({
        method: 'post',
        url: `roles/${val.id}/rights`,
        data: { rids: val.rids }
    })
    return data
}
const getdeslist = async () => {
    let data = await http({
        method: 'get',
        url: `rights/list`,
    })
    return data
}
const getgoodslist = async (val) => {
    let data = await http({
        method: 'get',
        url: `goods?${val}`,
    })
    return data
}
const goods_del = async (id) => {
    let data = await http({
        method: 'delete',
        url: `goods/${id}`,
    })
    return data
}
// 商品分类数据列表
const getCate = async (msg) => {
    // console.log(msg);
    let data = await http({
        url: `categories?type=3&pagenum=${msg.pagenum}&pagesize=${msg.pagesize}`,
        method: 'get',
        data: msg
    })
    return data
}
// 商品分类数据列表
const getCan = async (msg) => {
    // console.log(msg);
    let data = await http({
        url: `categories?type=2`,
        method: 'get',
    })
    return data
}
// 根据id查找分类
const souidCate = async (msg) => {
    // console.log(msg);
    let data = await http({
        url: `categories/${msg}`,
        method: 'get',
    })
    return data
}
// 修改提交分类
const tiCate = async (msg) => {
    console.log(msg);
    let data = await http({
        url: `categories/${msg.id}`,
        method: 'put',
        data: msg
    })
    return data
}
// 删除单个用户
const delCate = async (msg) => {
    console.log(msg);
    let data = await http({
        url: `categories/${msg}`,
        method: 'delete',
    })
    // console.log(data);
    return data
}
// 添加分类
const addCateList = async (msg) => {
    // console.log(msg);
    let data = await http({
        url: 'categories',
        method: 'post',
        data: msg
    })

    return data
}

// 商品分类数据列表
const getCatelist = async () => {
    // console.log(msg);
    let data = await http({
        url: `categories`,
        method: 'get',
    })
    return data
}
//分类参数列表
const getCatetab = async (val) => {
    // console.log(val);
    let data = await http({
        url: `categories/${val.id}/attributes?sel=${val.sel}`,
        method: 'get',
    })
    return data
}
//添加分类参数列表
const getCateAlllist = async (val) => {
    // console.log(val);
    let data = await http({
        url: `categories/${val.id}/attributes`,
        method: 'post',
        data: val
    })
    return data
}
const delCateAlllist = async (val) => {
    // console.log(val);
    let data = await http({
        url: `categories/${val.id}/attributes/${val.rid}`,
        method: 'delete',
    })
    return data
}
const setchildren = async (val) => {
    // console.log(val);
    let data = await http({
        url: `categories/${val.id}/attributes/${val.rid}`,
        method: 'put',
        data: val
    })
    return data
}

const addGoods = async (msg) => {
    console.log(msg);
    let data = await http({
        url: `goods?` + msg,
        method: 'post',
        data: msg
    })
    return data
}


// 获取参数列表
const getGories = async (msg) => {
    console.log(msg);
    let data = await http({
        url: `categories/${msg.id}/attributes?sel=${msg.sel}`,
        method: 'get',
        data:{attr_sel:msg.sel}
    })
    return data
}

const getorders  = async (val)=>{
    let data = await http({
        url:`orders?${val}`,
        method:'get'
    })
    return data
}
const getlist = async (msg) => {
    let data = await http({
        url: 'orders?' + msg,
        method: 'get',
        data: msg
    })
    return data
}


const logistics = async (id)=>{
    let data = await http({
        url:`/kuaidi/${id}`,
        method:'get'
    })
    return data
}
const getAlllist = async (msg)=>{
    let data = await http({
        url:'reports/type/1',
        method:'get'
    })
    return data
}
export {
    getlogin,
    getleft,
    getuser,
    postusers,
    deluser,
    setstatus,
    alteruser,
    getroles,
    setroles,
    getmenus,
    addroles,
    upuserRoles,
    delrole,
    delroledes,
    getdesAll,
    setdesall,
    getdeslist,
    getgoodslist,
    goods_del,
    getCate,
    getCan,
    souidCate,
    tiCate,
    delCate,
    addCateList,
    getCatelist,
    getCatetab,
    getCateAlllist,
    delCateAlllist,
    setchildren,
    addGoods,
    getGories,
    getorders,
    getlist,
    logistics,
    getAlllist
}