import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import VueQuillEditor from 'vue-quill-editor'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor)
Vue.config.productionTip = false
import ELEMENT from 'element-ui';  //引入element
import 'element-ui/lib/theme-chalk/index.css';  //引入element的样式
import ZkTable from 'vue-table-with-tree-grid'

Vue.filter('dateFormat', function (dateStr, pattern = '') {
  let dt = new Date(dateStr * 1000)
  let y = dt.getFullYear()
  let m = (dt.getMonth() + 1).toString().padStart(2, '0')
  let d = dt.getDate().toString().padStart(2, '0')
  pattern.toLowerCase() //传入参数转成小写
  if (pattern.toLowerCase() === 'yyyy-mm-dd') {
    return `${y}-${m}-${d}`
  } else {
    let hh = dt.getHours().toString().padStart(2, '0')
    let mm = dt.getMinutes().toString().padStart(2, '0')
    let ss = dt.getSeconds().toString().padStart(2, '0')
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
  }
})
Vue.component('tree-table', ZkTable)
Vue.use(ELEMENT);  //添加到vue实例


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
