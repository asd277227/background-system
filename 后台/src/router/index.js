import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/index',
    name: 'index',
    redirect: '/greet',
    component: () => import('../views/index.vue'),
    children: [
      {
        path: '/rights',
        name: 'about',
        component: () => import('../views/About.vue'),
      },
      {
        path: '/users',
        name: 'users',
        component: () => import('../views/users.vue'),
      },
      {
        path: '/roles',
        name: 'roles',
        component: () => import('../views/roles.vue'),
      },
      {
        path: '/greet',
        name: 'greet',
        component: () => import('../views/greet.vue'),
      },
      {
        path: '/goods',
        name: 'goods',
        component: () => import('../views/goods.vue'),
      },
      {
        path: '/params',
        name: 'params',
        component: () => import('../views/params.vue'),
      },
      {
        path: '/categories',
        name: 'categories',
        component: () => import('../views/categories.vue'),
      },
      {
        path: '/orders',
        name: 'orders',
        component: () => import('../views/orders.vue'),
      },
      {
        path: '/reports',
        name: 'reports',
        component: () => import('../views/reports.vue'),
      },
      {
        path: '/goods/add',
        name: 'add',
        component: () => import('../views/add.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  let token = localStorage.getItem("token")
  console.log(to);
  if (to.fullPath == "/") {
    next()  
  }else{
    if(token){
        next()
    }else{
      next('/')
    }
  }
})
export default router
